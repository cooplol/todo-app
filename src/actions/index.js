import { ADD_TASK, DELETE_TASK, EDIT_NICKNAME } from './actionTypes';

//action creators 
export const addTask = (name, nickname) => { 
  return {
    type: ADD_TASK,
    payload: { name, nickname }
  };
};

export const deleteTask = id => {
  return {
    type: DELETE_TASK,
    payload: id
  }
};

export const editNickname = (id, newNickname) => {
  return {
    type: EDIT_NICKNAME,
    payload: { id, newNickname }
  }
}
