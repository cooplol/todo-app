import React from 'react';
import TaskList from '../components/TaskList';
import AddTask from '../components/AddTask';

const MainPage = () => (
  <div className="wrapper wrapper--main-page">
    <AddTask />
    <TaskList />
  </div>
);

export default MainPage;