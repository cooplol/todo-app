import React from 'react';
import { Link } from 'react-router-dom';

import TaskDetails from '../components/TaskDetails';


const DetailsPage = props => (
  <div className="wrapper wrapper--details-page">
    <Link to='/'>Back to Main Page</Link>
    <div className="task-details-section">
      <TaskDetails id={props.match.params.id} />
    </div>
  </div>
);


export default DetailsPage;