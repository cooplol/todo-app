import React from 'react';
import { connect } from 'react-redux';
import { addTask } from '../actions';

const initState = {
  name: '',
  nickname: '',
  nameErrMsg: ''
};

class AddTask extends React.Component {
  state = initState;

  handleSubmit(event) {
    event.preventDefault();
    //get data to call handle addTask with 
    if (this.state.name === '') {
      this.setState({ nameErrMsg: 'Name cannot be empty.' });
      return;
    }

    //we have all data we need to create new task
    this.props.addTask(
      this.state.name,
      this.state.nickname ? this.state.nickname : this.state.name
    );

    this.setState(initState);
  }

  handleChange(event, name) {
    switch (name) {
      case 'name':
        this.setState({ name: event.target.value });
        break;

      case 'nickname':
        this.setState({ nickname: event.target.value });
        break;

      default:
    }

    this.setState({ nameErrMsg: '' });
  }

  render() {
    return (
      <div className="add-task-form-container">
        <h2 className="heading-secondary">Add a task</h2>
        <form className="add-task-form" onSubmit={event => this.handleSubmit(event)} >
          <div className="add-task-form__group">
            <label>Name:</label>
            <input className="add-task-form__input" name="name" type="text" value={this.state.name} onChange={event => this.handleChange(event, event.target.name)} />
            <div className="error">{this.state.nameErrMsg}</div>
          </div>
          <div className="add-task-form__group">
            <label>Nickname:</label>
            <input className="add-task-form__input" name="nickname" type="text" value={this.state.nickname} onChange={event => this.handleChange(event, event.target.name)} />
          </div>
          <div className="add-task-form__group">
            <button className="submit-btn">Add</button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(null, { addTask })(AddTask);