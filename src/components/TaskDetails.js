import React from 'react';
import { connect } from 'react-redux';
import { editNickname } from '../actions';

class TaskDetails extends React.Component {
  state = {
    newNickname: '',
    newNickNameErrMsg: '',
    givenID: this.props.id
  };

  handleNewNicknameChange = event => this.setState({
    newNickname: event.target.value,
    newNickNameErrMsg: ''
  });

  handleSubmitNewNickname = event => {
    event.preventDefault();

    if (this.state.newNickname === '') {
      this.setState({ newNickNameErrMsg: 'Nickname cannot be empty' });
    } else {
      this.props.editNickname(this.state.givenID, this.state.newNickname);
      this.setState({
        newNickname: '',
        newNickNameErrMsg: ''
      });
    }
  };

  renderDetails = () => {
    const list = this.props.taskList.filter(task => {
      if (task.id == this.state.givenID) {
        return task;
      } else { return false; }
    });

    const task = list[0];

    return (
      <div>
        <div className="task-details__group">
          <span className="task-details__label">ID:</span>
          <span>{task.id}</span>
        </div>
        <div className="task-details__group">
          <span className="task-details__label">Name:</span>
          <span className="task-details__value">{task.name}</span>
        </div>
        <div className="task-details__group">
          <span className="task-details__label">Creation Date:</span>
          <span className="task-details__value">{task.creationTime}</span>
        </div>
        <div className="task-details__group">
          <span className="task-details__label">Nickname:</span>
          <span className="task-details__value">{task.nickname.data.name}</span>
        </div>
        <div className="task-details__group">
          <form onSubmit={event => this.handleSubmitNewNickname(event)}>
            <label className="task-details__label">New Nickname:</label>
            <input className="task-details__value" name="newNickname" type="text" value={this.state.newNickname} onChange={event => this.handleNewNicknameChange(event)} />
            <button type="submit">Change</button>
          </form>
          <div className="error">{this.state.newNickNameErrMsg}</div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="task-details-container">
        <h2 className="heading-secondary">Task Details</h2>
        <div className="task-details">
          {this.renderDetails()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  taskList: state.taskListData.list
});

export default connect(
  mapStateToProps,
  { editNickname })(TaskDetails);