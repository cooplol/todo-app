import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { deleteTask } from '../actions';

const TaskItem = props => {
  const onDeleteClick = id => {
    props.deleteTask(id);
  };

  return (
    <li className="task-item">
      <span className="task-item__nickname">{props.nickname}</span>
      <Link to={`taskid/${props.id}`}>Details</Link>
      <button onClick={() => onDeleteClick(props.id)}>Delete</button>
    </li>
  );
};

export default connect(null, { deleteTask })(TaskItem);