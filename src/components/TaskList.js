import React from 'react';
import { connect } from 'react-redux';

import TaskItem from './TaskItem';


const TaskList = props => {
  const taskItems = props.taskList.map(task => {
    const id = task.id;
    const nickname = task.nickname.data.name;

    return <TaskItem key={id} nickname={nickname} id={id} />;
  });
  
  return (
    <ul className="task-list">
      {taskItems}
    </ul>
  );
};

const mapStateToProps = state => {
  return { taskList: state.taskListData.list };
};

export default connect(mapStateToProps)(TaskList);

