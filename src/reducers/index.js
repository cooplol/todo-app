import { combineReducers } from 'redux';

import { ADD_TASK, DELETE_TASK, EDIT_NICKNAME } from "../actions/actionTypes";
import { getCurrentFormattedDate } from '../utils/common';

const initTaskListData = {
  lastID: 0,
  list: []
};


const taskListDataReducer = (taskListData = initTaskListData,
  action) => {
  switch (action.type) {
    case ADD_TASK: {
      const newID = taskListData.lastID += 1;
      const newTask = {
        id: newID,
        name: action.payload.name,
        creationTime: getCurrentFormattedDate(),
        nickname: {
          data: {
            name: action.payload.nickname
          }
        }
      };
      return {
        lastID: newID,
        list: [...taskListData.list, newTask]
      }
    }

    case DELETE_TASK: {
      const idToDelete = action.payload;
      const newList = taskListData.list.filter(task => {
        if (task.id !== idToDelete) { 
          return task; 
        } else { return false; }
      });

      return {
        lastID: taskListData.lastID,
        list: newList
      }
    }

    case EDIT_NICKNAME: {
      const idToEdit = action.payload.id;
      const newList = taskListData.list.map(task => {
        if (task.id == idToEdit) {
          task.nickname.data.name = action.payload.newNickname;
        }
        return task;
      });

      return {
        lastID: taskListData.lastID,
        list: newList
      }
    }

    default:
      return taskListData;
  }
};

export default combineReducers({ taskListData: taskListDataReducer });