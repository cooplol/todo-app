import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import MainPage from './pages/MainPage';
import DetailsPage from './pages/DetailsPage';

const App = () => (
  <BrowserRouter>
    <div>
      <Route path="/" exact component={MainPage} />
      <Route path="/taskid/:id(\d+)" exact component={DetailsPage} />
    </div>
  </BrowserRouter>
);

export default App;