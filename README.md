This is an assignment to demonstrate usage of Redux in a React app.

## App Details

### How to Run this App

This app was created with create-react-app.  To run this app perform the following steps:

* If you don't already have npm installed on your computer, install [npm](https://www.npmjs.com/get-npm).

* Clone the Git repository for this app into a new directory in your local environment.

* Open a command prompt and navigate to the directory containing this app where the 'package.json' file is located.

* If this is your first time running this app, you should install all necessary packages by typing 'npm i' (without quotes) in the command line.

* To excute this code in a local environment, type 'npm start' (without quotes) in the command line.  

* Running 'npm start' should start a local web server, which will allow you to view this app by navigating to http://localhost:3000/ in your web browser.  More details can be found in 'README_REACT_APP.md'

### Original Assignment Details

Assignment

Create a Simple "TODO" web application using React + Redux. 
Features:

1. Page 1 : Display list of Tasks in the UI
2. Page 1 : User should allow to add task to list
3. Page 1 : User should allow to delete task from list
4. Page 2 : Create a detail page to display selected task (Display id, name, creation date, and nickname)
5. Page 2 : User should allow to modify/update the task's nickname. (When user navigate back to Page 1, user should see the change.)


Data structure:
let task = {
	id: "1",
	name: "Submit assignemnt",
	creationTime: "2019-01-23",
	nickname: {
		data:{
			name: "React assignment"
		}
	}
}

NOTE: Page 2 should retrieve data from Redux store. ( Passing "task" data via Session Storage or URL parameters is not allowed. However, you can pass task's id in the url, such as localhost:3000/page2?id=1 )

Please submit your code to github/bitbucket (Prefer Bitbucket).

Please also provide the instruction how to execute the code in local environment.

Due date: 3 days.

### Additional Assignment Details

There were some details not explicitly defined in the original assignment details, so I made and implemented some assumptions on the specifications of this assignment.  Here are additional specifications that have been implemented for this assignment:

* On the main page (page 1), creating a new task requires a Name.  A Nickname is optional.  If no Nickname is provided, the task's Nickname will be the same as its Name.

* On the main page (page 1), an existing task in the list will use its Nickname as its display name (the task's Name can be seen on its details page).

* On the task details (page 2), a task's Nickname cannot be set to an empty string. 

